# Galaxy cluster mass recovery

Link to Notion: https://www.notion.so/Galaxy-clusters-4c2f6ca37ffe412dbda1daa94f6adc40

### Team:

**LAMBDA:** *Ustyuzhanin A., Zarodnyuk A., Trofimova E.*

**MSU:** *Katkov I., Chiligarian I., Toptun V.*

**HSE students:** *Gradoboev D., Solovyev A., Atymkhanova M., Rog A., Troshin D.*

### Task:
- To detect and label galaxy clusters 
- To estimate the mass of each cluster

## Dataset description

Contains information on about 4 millions of galaxies

**Features**

- about 560 features (columns)

Here one may find a description to the dataset: 

[extragalactic _ UV-to-NIR catalog _ wiki _ Catalogs Information RCSED v2 — Bitbucket.html](https://s3-us-west-2.amazonaws.com/secure.notion-static.com/b6a2bf3c-f544-422d-bc97-cd74a475ae20/extragalactic___UV-to-NIR_catalog___wiki___Catalogs_Information_RCSED_v2__Bitbucket.html)

**Most meaningful features:** 

- **ra**  is the angle of right ascension  (from 0 to 360 degrees)
- **dec** is the angle of declination ( from -90 to +90 degrees)
- **z** is the redshift (positive number) !!! *pay your attention on **z_sdss** that duplicates values of **z** that belong to SDSS observations and has NaN in other.*
- **petromag_r_sdss** (under the consideration)
- **petromag_g_sdss** (under the consideration)
